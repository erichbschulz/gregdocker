Experiments with Docker, Node 6, Typescript, Angular 2 and Yeoman

Using

* https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
* https://hub.docker.com/_/node/
* http://yeoman.io/


docker build -t erich/gregory .

docker run -p 49160:3000 -p 49161:3001 -it -v $(pwd)/app:/usr/src/app erich/gregory /bin/bash

#Print the output of your app:
# Get container ID
$ docker ps
# Print app output
$ docker logs <container id>

# Example
Running on http://localhost:8080

If you need to go inside the container you can use the exec command:

# Enter the container
$ docker exec -it <container id> /bin/bash
Test

To test your app, get the port of your app that Docker mapped:

$ docker ps

curl -i localhost:49160
