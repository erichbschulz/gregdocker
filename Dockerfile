FROM node:boron

RUN npm install -g yo
# Create app directory
WORKDIR /usr/src/app

RUN mkdir -p /usr/src/app \
      && chown node:node /usr/src/app \
      && chown node:node /usr/local/lib/node_modules

VOLUME /usr/src/app
WORKDIR /usr/src/app
USER node
RUN npm install -g generator-fountain-angular2

# CMD [ "npm", "start" ]
